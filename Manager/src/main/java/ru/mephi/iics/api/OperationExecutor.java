package ru.mephi.iics.api;

import ru.mephi.iics.adapters.Adapters;
import ru.mephi.iics.proto.WorkerProto;
import ru.mephi.iics.helpers.MatrixСonverter;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;

public class OperationExecutor {
    private final Adapters adapter;

    public OperationExecutor(){
        this.adapter = new Adapters();
    }

    private int[][] getMultiplication(int[][] firstMatrix, int[][] secondMatrix) throws InterruptedException {
        var uuid = UUID.randomUUID().toString();
        var job =
                WorkerProto.WorkerRq.newBuilder().setRqData(MatrixСonverter.getMatrixPair(firstMatrix, secondMatrix)).setCorrelationId(uuid).build();
        var cdl = new CountDownLatch(1);
        adapter.kafka.putMessage(job, cdl);
        cdl.await();
        return MatrixСonverter.getSimpleMatrix(adapter.kafka.extractResponse(uuid));
    }

    public CompletableFuture<int[][]> executeMultiplication(int[][] firstMatrix, int[][] secondMatrix){
        return CompletableFuture
                .supplyAsync(() -> {
                    try {
                        return this.getMultiplication(firstMatrix, secondMatrix);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return null;
                });
    }
}
