package ru.mephi.iics.adapters;

import com.github.daniel.shuy.kafka.protobuf.serde.KafkaProtobufDeserializer;
import com.github.daniel.shuy.kafka.protobuf.serde.KafkaProtobufSerializer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import ru.mephi.iics.proto.MatrixProto;
import ru.mephi.iics.proto.WorkerProto;


public class KafkaAdapter {
    private final ConcurrentHashMap<String, CountDownLatch> resultWaiting;
    private final ConcurrentHashMap<String, MatrixProto.Matrix> resultData;
    private KafkaProducer<String, WorkerProto.WorkerRq> producer;
    private KafkaConsumer<String, WorkerProto.WorkerRs> consumer;
    private String bootstrapServers;
    private String rsTopic;
    public String rqTopic;
    private String groupId;
    private Thread consumingThread;


    public KafkaAdapter() {
        resultWaiting = new ConcurrentHashMap<>();
        resultData = new ConcurrentHashMap<>();
    }

    private void extractEnvVars() {
        groupId = System.getenv("MSS_KAFKA_GROUP_ID");
        if (groupId == null) {
            groupId = "mss.workers";
        }

        rsTopic = System.getenv("MSS_KAFKA_RS_TOPIC");
        if (rsTopic == null) {
            rsTopic = "mss.worker.rs";
        }

        bootstrapServers = System.getenv("MSS_KAFKA_BOOTSTRAP_SERVERS");
        if (bootstrapServers == null) {
            bootstrapServers = "localhost:29092";
        }

        rqTopic = System.getenv("MSS_KAFKA_RQ_TOPIC");
        if (rqTopic == null) {
            rqTopic = "mss.worker.rq";
        }
    }

    private KafkaProducer<String, WorkerProto.WorkerRq> generateProducer() {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", bootstrapServers);
        return new KafkaProducer<>(properties, new StringSerializer(),
                new KafkaProtobufSerializer<>());
    }

    private KafkaConsumer<String, WorkerProto.WorkerRs> generateConsumer() {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", bootstrapServers);
        properties.setProperty("group.id", groupId);
        properties.setProperty("auto.offset.reset","latest");
        return new KafkaConsumer<>(properties,
                new StringDeserializer(),
                new KafkaProtobufDeserializer<>(WorkerProto.WorkerRs.parser()));
    }

    public MatrixProto.Matrix extractResponse(String uuid){
        try{
            var res = resultData.get(uuid);
            resultData.remove(uuid);
            return res;
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void putMessage(WorkerProto.WorkerRq request, CountDownLatch cdl) {
        resultWaiting.put(request.getCorrelationId(), cdl);
        try {
            producer.send(new ProducerRecord<>(rqTopic, null, request));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() {
        extractEnvVars();

        consumer = generateConsumer();
        consumer.subscribe(Collections.singleton(rsTopic));
        producer = generateProducer();

        consumingThread = new Thread(() -> {
            while (true) {
                ConsumerRecords<String, WorkerProto.WorkerRs> records = consumer.poll(Duration.ofMillis(1000));
                if (records.count() == 0) {
                    continue;
                }
                for (ConsumerRecord<String, WorkerProto.WorkerRs> record : records) {
                    var uuid = record.value().getCorrelationId();
                    resultData.put(uuid, record.value().getRsData());
                    if(resultWaiting.containsKey(uuid))
                        resultWaiting.get(uuid).countDown();
                    resultWaiting.remove(uuid);
                }
            }
        });

        consumingThread.setDaemon(true);

        consumingThread.start();

    }

}


