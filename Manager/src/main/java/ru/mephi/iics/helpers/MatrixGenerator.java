package ru.mephi.iics.helpers;

import java.util.Random;

import static java.lang.Math.abs;

public class MatrixGenerator {
    public static int[][] createRandomMatrix(int n) {
        Random random = new Random();
        int[][] matrix = new int[n][n];
        for(int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = random.nextInt(1000);
            }
        }
        return matrix;
    }

    public static int[][] createRandomMatrix(int n, int bound) {
        Random random = new Random();
        int[][] matrix = new int[n][n];
        for(int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = random.nextInt(abs(2*bound)) - bound;
            }
        }
        return matrix;
    }

    public static int[][] createRandomMatrix(int n, int m, int bound) {
        Random random = new Random();
        int[][] matrix = new int[n][m];
        for(int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                matrix[i][j] = random.nextInt(abs(2*bound)) - bound;
            }
        }
        return matrix;
    }
}
