package ru.mephi.iics.helpers;

import ru.mephi.iics.proto.MatrixProto;

public class MatrixСonverter {
    public static int[][] getSimpleMatrix(MatrixProto.Matrix matrix){
        var dimX = matrix.getDimX();
        var dimY = matrix.getDimY();
        int[][] res = new int[dimX][dimY];
        for (var i = 0; i < dimX; ++i)
        {
            for (var j = 0; j < dimY; ++j)
            {
                res[i][j] = matrix.getLines(i).getColumns(j);
            }
        }
        return res;
    }

    public static MatrixProto.Matrix getProtoMatrix(int[][] matrix){
        var resultBuilder = MatrixProto.Matrix.newBuilder().setDimX(matrix.length).setDimY(matrix[0].length);
        for (var i = 0; i < matrix.length; ++i)
        {
            var lineBuilder = MatrixProto.Matrix.Line.newBuilder();
            for (var j = 0; j < matrix[0].length; ++j)
            {
                lineBuilder.addColumns(matrix[i][j]);
            }
            resultBuilder.addLines(lineBuilder.build());
        }
        return resultBuilder.build();
    }

    public static MatrixProto.PairMatrix getMatrixPair(int[][] firstMatrix, int[][] secondMatrix){
        return MatrixProto.PairMatrix.newBuilder().setLeft(getProtoMatrix(firstMatrix)).setRight(getProtoMatrix(secondMatrix)).build();
    }
}
