package ru.mephi.iics.console;

import org.apache.commons.cli.ParseException;
import ru.mephi.iics.api.OperationExecutor;
import ru.mephi.iics.helpers.MatrixGenerator;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static java.lang.Math.abs;

public class ConsoleInterface {

    private static CMDConfiguration config;

    private static int[][] matrix, secondMatrix;

    public static void main(String[] args) throws ParseException, InterruptedException, ExecutionException {
        config = new CMDConfiguration(args);
        printConfiguration();
        fillMultiplyObjects();
        OperationExecutor operationExecutor = new OperationExecutor();
        executeOperation(() -> operationExecutor.executeMultiplication(matrix, secondMatrix));
    }

    private static void printConfiguration(){
        System.out.println("Количество строк: " + config.getRowsCount());
        System.out.println("Количество столбцов: " + config.getColsCount());
        if(config.autoGeneration()){
            System.out.println("Значения в ячейках матрицы сгенерированы в диапазоне: [" + -abs(config.getBound()) +
                    ", " + abs(config.getBound()) + "]");
        }
        System.out.println("Количество исследуемых итераций: " + config.getIterationsCount());
    }

    private static void fillMultiplyObjects(){
        if(!config.autoGeneration()){
            Scanner in = new Scanner(System.in);
            System.out.println("Введите матрицу которую хотите умножить");
            matrix = new int[config.getRowsCount()][config.getColsCount()];
            for(int row = 0; row < config.getRowsCount(); ++row){
                for(int coll = 0; coll < config.getColsCount(); ++coll){
                    matrix[row][coll] = in.nextInt();
                }
            }
            System.out.println("Введите матрицу на которую хотите умножить");
            secondMatrix = new int[config.getColsCount()][config.getRowsCount()];
            for(int row = 0; row < config.getColsCount(); ++row){
                for(int coll = 0; coll < config.getRowsCount(); ++coll){
                    secondMatrix[row][coll] = in.nextInt();
                }
            }
        }
        else{
            matrix = MatrixGenerator.createRandomMatrix(config.getRowsCount(), config.getColsCount(), config.getBound());
            secondMatrix = MatrixGenerator.createRandomMatrix(config.getColsCount(), config.getRowsCount(), config.getBound());
        }
    }

    private static int[][] executeOperation(Returning operation) throws InterruptedException, ExecutionException {
        ArrayList<CompletableFuture<int[][]> > totalRes = new ArrayList<>(config.getIterationsCount());
        long avt = System.currentTimeMillis();
        for(int i = 0; i < config.getIterationsCount(); ++i){
            CompletableFuture<int[][]> result;
            totalRes.add(operation.result());
        }
        int[][] result = null;
        for(var elem: totalRes){
            result = elem.get();
            //System.out.println(1);
        }
        avt = System.currentTimeMillis() - avt;
        if(config.toPrintRes()) {
            printMatrix(result);
        }
        System.out.println("Среднее время выполнения итераций: " + ((double) avt / (double) config.getIterationsCount()) + " миллисекунд");
        System.out.println("Общее время выполнения итераций: " + avt + " миллисекунд");
        return result;
    }

    private static void printMatrix(int[][] matrix){
        System.out.println("Результат умножения матрицы:");
        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {
                System.out.printf("%10d", matrix[row][col]);
            }
            System.out.println();
        }
    }

    private interface Returning {
        public CompletableFuture<int[][]> result() throws InterruptedException;
    }

    private static int getRandomInt(int bound){
        Random random = new Random();
        return random.nextInt(-abs(bound), abs(bound));
    }
}
