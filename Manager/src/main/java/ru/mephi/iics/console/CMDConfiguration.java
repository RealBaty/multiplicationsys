package ru.mephi.iics.console;

import org.apache.commons.cli.*;

public class CMDConfiguration {
    private final CommandLine commandLine;

    public CMDConfiguration(String[] args) throws ParseException {
        Options options = new Options();
        options.addOption("a", "auto", false, "use auto generate matrix");
        options.addOption("n", true, "count of rows in first matrices");
        options.addOption("m", true, "count of cols in first matrices");
        options.addOption("i", "iterations", true, "count of main iterations");
        options.addOption("b", "bound", true, "bound of generation elements");
        options.addOption("p", "print_result", false, "to print the result of the multiplication console");
        CommandLineParser cmdLinePosixParser = new DefaultParser();
        commandLine = cmdLinePosixParser.parse(options, args);
    }

    public boolean toPrintRes(){
        return commandLine.hasOption("p");
    }

    public boolean autoGeneration(){
        return commandLine.hasOption("a");
    }

    public int getRowsCount(){
        if(!commandLine.hasOption("n")){
            return 3;
        }
        return Integer.parseInt(commandLine.getOptionValue("n"));
    }

    public int getColsCount(){
        if(!commandLine.hasOption("m")){
            return getRowsCount();
        }
        return Integer.parseInt(commandLine.getOptionValue("m"));
    }

    public int getIterationsCount(){
        if(!commandLine.hasOption("i")){
            return 1;
        }
        var res = Integer.parseInt(commandLine.getOptionValue("i"));
        if(res <= 0)
            res = 1;
        return res;
    }

    public int getBound(){
        if(!commandLine.hasOption("b")){
            return 1000;
        }
        return Integer.parseInt(commandLine.getOptionValue("b"));
    }
}
