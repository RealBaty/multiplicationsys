package ru.mephi.iics.proto;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.44.0)",
    comments = "Source: manager.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class MatrixSumServiceGrpc {

  private MatrixSumServiceGrpc() {}

  public static final String SERVICE_NAME = "MatrixSumService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<ru.mephi.iics.proto.MatrixProto.PairMatrix,
      ru.mephi.iics.proto.MatrixProto.Matrix> getSumMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "Sum",
      requestType = ru.mephi.iics.proto.MatrixProto.PairMatrix.class,
      responseType = ru.mephi.iics.proto.MatrixProto.Matrix.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<ru.mephi.iics.proto.MatrixProto.PairMatrix,
      ru.mephi.iics.proto.MatrixProto.Matrix> getSumMethod() {
    io.grpc.MethodDescriptor<ru.mephi.iics.proto.MatrixProto.PairMatrix, ru.mephi.iics.proto.MatrixProto.Matrix> getSumMethod;
    if ((getSumMethod = MatrixSumServiceGrpc.getSumMethod) == null) {
      synchronized (MatrixSumServiceGrpc.class) {
        if ((getSumMethod = MatrixSumServiceGrpc.getSumMethod) == null) {
          MatrixSumServiceGrpc.getSumMethod = getSumMethod =
              io.grpc.MethodDescriptor.<ru.mephi.iics.proto.MatrixProto.PairMatrix, ru.mephi.iics.proto.MatrixProto.Matrix>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "Sum"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ru.mephi.iics.proto.MatrixProto.PairMatrix.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ru.mephi.iics.proto.MatrixProto.Matrix.getDefaultInstance()))
              .setSchemaDescriptor(new MatrixSumServiceMethodDescriptorSupplier("Sum"))
              .build();
        }
      }
    }
    return getSumMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static MatrixSumServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<MatrixSumServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<MatrixSumServiceStub>() {
        @java.lang.Override
        public MatrixSumServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new MatrixSumServiceStub(channel, callOptions);
        }
      };
    return MatrixSumServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static MatrixSumServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<MatrixSumServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<MatrixSumServiceBlockingStub>() {
        @java.lang.Override
        public MatrixSumServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new MatrixSumServiceBlockingStub(channel, callOptions);
        }
      };
    return MatrixSumServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static MatrixSumServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<MatrixSumServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<MatrixSumServiceFutureStub>() {
        @java.lang.Override
        public MatrixSumServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new MatrixSumServiceFutureStub(channel, callOptions);
        }
      };
    return MatrixSumServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class MatrixSumServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void sum(ru.mephi.iics.proto.MatrixProto.PairMatrix request,
        io.grpc.stub.StreamObserver<ru.mephi.iics.proto.MatrixProto.Matrix> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSumMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getSumMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                ru.mephi.iics.proto.MatrixProto.PairMatrix,
                ru.mephi.iics.proto.MatrixProto.Matrix>(
                  this, METHODID_SUM)))
          .build();
    }
  }

  /**
   */
  public static final class MatrixSumServiceStub extends io.grpc.stub.AbstractAsyncStub<MatrixSumServiceStub> {
    private MatrixSumServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MatrixSumServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new MatrixSumServiceStub(channel, callOptions);
    }

    /**
     */
    public void sum(ru.mephi.iics.proto.MatrixProto.PairMatrix request,
        io.grpc.stub.StreamObserver<ru.mephi.iics.proto.MatrixProto.Matrix> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getSumMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class MatrixSumServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<MatrixSumServiceBlockingStub> {
    private MatrixSumServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MatrixSumServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new MatrixSumServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public ru.mephi.iics.proto.MatrixProto.Matrix sum(ru.mephi.iics.proto.MatrixProto.PairMatrix request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getSumMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class MatrixSumServiceFutureStub extends io.grpc.stub.AbstractFutureStub<MatrixSumServiceFutureStub> {
    private MatrixSumServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MatrixSumServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new MatrixSumServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<ru.mephi.iics.proto.MatrixProto.Matrix> sum(
        ru.mephi.iics.proto.MatrixProto.PairMatrix request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getSumMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_SUM = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final MatrixSumServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(MatrixSumServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_SUM:
          serviceImpl.sum((ru.mephi.iics.proto.MatrixProto.PairMatrix) request,
              (io.grpc.stub.StreamObserver<ru.mephi.iics.proto.MatrixProto.Matrix>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class MatrixSumServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    MatrixSumServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ru.mephi.iics.proto.ManagerProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("MatrixSumService");
    }
  }

  private static final class MatrixSumServiceFileDescriptorSupplier
      extends MatrixSumServiceBaseDescriptorSupplier {
    MatrixSumServiceFileDescriptorSupplier() {}
  }

  private static final class MatrixSumServiceMethodDescriptorSupplier
      extends MatrixSumServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    MatrixSumServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (MatrixSumServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new MatrixSumServiceFileDescriptorSupplier())
              .addMethod(getSumMethod())
              .build();
        }
      }
    }
    return result;
  }
}
