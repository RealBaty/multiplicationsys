package ru.mephi.iics.algos;

import ru.mephi.iics.thread.ThreadPool;

import java.util.Random;

import static java.lang.Math.abs;

public class MatrixOperations {

    private int threadsCount;

    public MatrixOperations(int threadsCount){
        this.threadsCount = threadsCount;
    }

    public MatrixOperations(){
        this.threadsCount = 2;
    }

    public static int[][] createRandomMatrix(int n) {
        Random random = new Random();
        int[][] matrix = new int[n][n];
        for(int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = random.nextInt(1000);
            }
        }
        return matrix;
    }

    public static int[][] createRandomMatrix(int n, int bound) {
        Random random = new Random();
        int[][] matrix = new int[n][n];
        for(int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = random.nextInt(-abs(bound), abs(bound));
            }
        }
        return matrix;
    }

    public void useTreads(int x){
        threadsCount = x;
    }

    private int multiplyMatricesCell(int[][] firstMatrix, int[][] secondMatrix, int row, int col) {
        int cell = 0;
        for (int i = 0; i < secondMatrix.length; i++) {
            cell += firstMatrix[row][i] * secondMatrix[i][col];
        }
        return cell;
    }

    private void setCell(int[][] res, int[][] firstMatrix, int[][] secondMatrix, int row, int col){
        res[row][col] = multiplyMatricesCell(firstMatrix, secondMatrix, row, col);
    }

    private void setCell(int[][] res, int[][] matrix, int n, int row, int col){
        res[row][col] = matrix[row][col] * n;
    }

    private void setRow(int[][] result, int[][] firstMatrix, int[][] secondMatrix, int row){
        for (int col = 0; col < result[row].length; col++) {
            setCell(result, firstMatrix, secondMatrix, row, col);
        }
    }

    private void setRow(int[][] result, int[][] matrix, int n, int row){
        for (int col = 0; col < result[row].length; col++) {
            setCell(result, matrix, n, row, col);
        }
    }

    private void saveMultiplyMatricesCellGroup(int[][] result, int[][] firstMatrix, int[][] secondMatrix, int beginIndex, int endIndex){
        int rowLen = secondMatrix[0].length;
        for (int index = beginIndex; index <= endIndex; index++) {
            setCell(result, firstMatrix, secondMatrix, index / rowLen, index % rowLen);
        }
    }

    private void saveMultiplyMatricesCellGroup(int[][] result, int[][] matrix, int n, int beginIndex, int endIndex){
        int rowLen = matrix[0].length;
        for (int index = beginIndex; index <= endIndex; index++) {
            setCell(result, matrix, n, index / rowLen, index % rowLen);
        }
    }

    public int[][] multiplyMatrices(int[][] firstMatrix, int[][] secondMatrix) {
        int[][] result = new int[firstMatrix.length][secondMatrix[0].length];

        for (int row = 0; row < result.length; row++) {
            for (int col = 0; col < result[row].length; col++) {
                setCell(result, firstMatrix, secondMatrix, row, col);
            }
        }
        return result;
    }

    public int[][] multiplyMatricesMultiTreadsCell(int[][] firstMatrix, int[][] secondMatrix) throws InterruptedException {
        int[][] result = new int[firstMatrix.length][secondMatrix[0].length];
        ThreadPool threadPool = new ThreadPool(threadsCount);

        for (int row = 0; row < result.length; row++) {
            for (int col = 0; col < result[row].length; col++) {
                final int finalCol = col;
                final int finalRow = row;
                threadPool.execute(() -> setCell(result, firstMatrix, secondMatrix, finalRow, finalCol));
            }
        }

        threadPool.doAllAndShutdown();
        return result;
    }

    public int[][] multiplyMatricesMultiTreadsRow(int[][] firstMatrix, int[][] secondMatrix) throws InterruptedException {
        int[][] result = new int[firstMatrix.length][secondMatrix[0].length];
        ThreadPool threadPool = new ThreadPool(threadsCount);

        for (int row = 0; row < result.length; row++) {
            final int finalRow = row;
            threadPool.execute(() -> setRow(result, firstMatrix, secondMatrix, finalRow));
        }

        threadPool.doAllAndShutdown();
        return result;
    }

    public int[][] multiplyMatricesMultiTreadsMultiCells(int[][] firstMatrix, int[][] secondMatrix, int cellsCount) throws InterruptedException {
        int[][] result = new int[firstMatrix.length][secondMatrix[0].length];
        int cellsCountTotal = firstMatrix.length * secondMatrix[0].length;
        ThreadPool threadPool = new ThreadPool(threadsCount);

        for(int beginIndex = 0; beginIndex < cellsCountTotal; beginIndex = beginIndex + cellsCount){
            int endIndex = beginIndex + cellsCount - 1;
            if (endIndex >= cellsCountTotal){
                endIndex = cellsCountTotal - 1;
            }
            int finalEndIndex = endIndex;
            final int finalBeginIndex = beginIndex;
            threadPool.execute(() -> saveMultiplyMatricesCellGroup(result, firstMatrix, secondMatrix, finalBeginIndex, finalEndIndex));
        }

        threadPool.doAllAndShutdown();
        return result;
    }

    public int[][] multiplyMatrixByNum(int[][] matrix, int n){
        int[][] result = new int[matrix.length][matrix[0].length];

        for (int row = 0; row < result.length; row++) {
            for (int col = 0; col < result[row].length; col++) {
                setCell(result, matrix, n, row, col);
            }
        }
        return result;
    }

    public int[][] multiplyMatrixByNumMultiTreadsCell(int[][] matrix, int n) throws InterruptedException {
        int[][] result = new int[matrix.length][matrix[0].length];
        ThreadPool threadPool = new ThreadPool(threadsCount);

        for (int row = 0; row < result.length; row++) {
            for (int col = 0; col < result[row].length; col++) {
                final int finalCol = col;
                final int finalRow = row;
                threadPool.execute(() -> setCell(result, matrix, n, finalRow, finalCol));
            }
        }

        threadPool.doAllAndShutdown();
        return result;
    }

    public int[][] multiplyMatrixByNumMultiTreadsRow(int[][] matrix, int n) throws InterruptedException {
        int[][] result = new int[matrix.length][matrix[0].length];
        ThreadPool threadPool = new ThreadPool(threadsCount);

        for (int row = 0; row < result.length; row++) {
            final int finalRow = row;
            threadPool.execute(() -> setRow(result, matrix, n, finalRow));
        }

        threadPool.doAllAndShutdown();
        return result;
    }

    public int[][] multiplyMatrixByNumMultiTreadsMultiCells(int[][] matrix, int n, int cellsCount) throws InterruptedException {
        int[][] result = new int[matrix.length][matrix[0].length];
        int cellsCountTotal = matrix.length * matrix[0].length;
        ThreadPool threadPool = new ThreadPool(threadsCount);

        for(int beginIndex = 0; beginIndex < cellsCountTotal; beginIndex = beginIndex + cellsCount){
            int endIndex = beginIndex + cellsCount - 1;
            if (endIndex >= cellsCountTotal){
                endIndex = cellsCountTotal - 1;
            }
            int finalEndIndex = endIndex;
            final int finalBeginIndex = beginIndex;
            threadPool.execute(() -> saveMultiplyMatricesCellGroup(result, matrix, n, finalBeginIndex, finalEndIndex));
        }

        threadPool.doAllAndShutdown();
        return result;
    }

}
