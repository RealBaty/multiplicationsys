package ru.mephi.iics.algos;

import org.apache.commons.cli.ParseException;

import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

import static java.lang.Math.abs;

public class ConsoleInterface {

    private static CMDConfiguration config;

    private static int[][] matrix, secondMatrix;

    private static int c;

    public static void main(String[] args) throws ParseException, InterruptedException {
        config = new CMDConfiguration(args);
        printConfiguration();
        fillMultiplyObjects();
        performMultiplication();
    }

    private static void printConfiguration(){
        System.out.println("Размерность матрицы: " + config.getMatrixSize());
        if(config.autoGeneration()){
            System.out.println("Значения в ячейках матрицы сгенерированы в диапазоне: [" + -abs(config.getBound()) +
                    ", " + abs(config.getBound()) + "]");
        }
        System.out.println("Производится умножение на: " + config.getMultiplicationType());
        System.out.println("Используемый метод умножения: " + config.getMultiplicationMethod());
        if(Objects.equals(config.getMultiplicationMethod(), "thread_by_cell_group")){
            System.out.println("Количество ячеек в каждой задаче исполняемой потоками: " + config.getCellGroup());
        }
        System.out.println("Количество используемых потоков: " + config.getThreadCount());
        System.out.println("Количество итераций для разогрева JVM: " + config.getWarmupCount());
        System.out.println("Количество исследуемых итераций: " + config.getIterationsCount());
    }

    private static void fillMultiplyObjects(){
        if(!config.autoGeneration()){
            Scanner in = new Scanner(System.in);
            System.out.println("Введите матрицу которую хотите умножить");
            matrix = new int[config.getMatrixSize()][config.getMatrixSize()];
            for(int row = 0; row < config.getMatrixSize(); ++row){
                for(int coll = 0; coll < config.getMatrixSize(); ++coll){
                    matrix[row][coll] = in.nextInt();
                }
            }
            if(Objects.equals(config.getMultiplicationType(), "matrix")){
                System.out.println("Введите матрицу на которую хотите умножить");
                secondMatrix = new int[config.getMatrixSize()][config.getMatrixSize()];
                for(int row = 0; row < config.getMatrixSize(); ++row){
                    for(int coll = 0; coll < config.getMatrixSize(); ++coll){
                        secondMatrix[row][coll] = in.nextInt();
                    }
                }
            }
            else{
                System.out.println("ведите число на которое хотите умножить");
                c = in.nextInt();
            }
        }
        else{
            matrix = MatrixOperations.createRandomMatrix(config.getMatrixSize(), config.getBound());
            if(Objects.equals(config.getMultiplicationType(), "matrix")){
                secondMatrix = MatrixOperations.createRandomMatrix(config.getMatrixSize(), config.getBound());
            }
            else{
                c = getRandomInt(config.getBound());
            }
        }
    }

    private static void executeOperation(Returning operation) throws InterruptedException {
        boolean printRes = !config.autoGeneration();
        long avt = 0;
        for(int i = 0; i < config.getWarmupCount() + config.getIterationsCount(); ++i){
            int[][] result;
            if(i >= config.getWarmupCount()){
                long time = System.currentTimeMillis();
                result = operation.result();
                avt = avt + (System.currentTimeMillis() - time);
            }
            else {
                result = operation.result();
            }
            if(printRes && (i == 0)){
                System.out.println("Результат умножения матрицы:");
                for (int row = 0; row < result.length; row++) {
                    for (int col = 0; col < result[row].length; col++) {
                        System.out.printf("%10d", result[row][col]);
                    }
                    System.out.println();
                }
            }
        }
        System.out.println("Среднее время выполнения итераций: " + ((double) avt / (double) config.getIterationsCount()) + " миллисекунд");
        System.out.println("Общее время выполнения итераций: " + avt + " миллисекунд");
    }

    private static void performMultiplication() throws InterruptedException {
        MatrixOperations matrixOperations = new MatrixOperations(config.getThreadCount());
        if(Objects.equals(config.getMultiplicationType(), "matrix")){
            if(Objects.equals(config.getMultiplicationMethod(), "single_thread")){
                executeOperation(() -> matrixOperations.multiplyMatrices(matrix, secondMatrix));
            }
            if(Objects.equals(config.getMultiplicationMethod(), "thread_by_cell")){
                executeOperation(() -> matrixOperations.multiplyMatricesMultiTreadsCell(matrix, secondMatrix));
            }
            if(Objects.equals(config.getMultiplicationMethod(), "thread_by_row")){
                executeOperation(() -> matrixOperations.multiplyMatricesMultiTreadsRow(matrix, secondMatrix));
            }
            if(Objects.equals(config.getMultiplicationMethod(), "thread_by_cell_group")){
                executeOperation(() -> matrixOperations.multiplyMatricesMultiTreadsMultiCells(matrix, secondMatrix,
                        config.getCellGroup()));
            }
        }
        else {
            if(Objects.equals(config.getMultiplicationType(), "number")){
                if(Objects.equals(config.getMultiplicationMethod(), "single_thread")){
                    executeOperation(() -> matrixOperations.multiplyMatrixByNum(matrix, c));
                }
                if(Objects.equals(config.getMultiplicationMethod(), "thread_by_cell")){
                    executeOperation(() -> matrixOperations.multiplyMatrixByNumMultiTreadsCell(matrix, c));
                }
                if(Objects.equals(config.getMultiplicationMethod(), "thread_by_row")){
                    executeOperation(() -> matrixOperations.multiplyMatrixByNumMultiTreadsRow(matrix, c));
                }
                if(Objects.equals(config.getMultiplicationMethod(), "thread_by_cell_group")){
                    executeOperation(() -> matrixOperations.multiplyMatrixByNumMultiTreadsMultiCells(matrix, c,
                            config.getCellGroup()));
                }
            }
        }
    }

    private interface Returning {
        public int[][] result() throws InterruptedException;
    }

    private static int getRandomInt(int bound){
        Random random = new Random();
        return random.nextInt(-abs(bound), abs(bound));
    }
}
