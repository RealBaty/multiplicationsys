package ru.mephi.iics.algos;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.*;

import java.util.Objects;

public class CMDConfiguration {
    private final CommandLine commandLine;

    public CMDConfiguration(String[] args) throws ParseException {
        Options options = new Options();
        options.addOption("a", "auto", false, "use auto generate matrix");
        options.addOption("n", true, "count of rows and cols in matrices");
        options.addOption("t", "type", true, "type of matrix multiplication (number or matrix)");
        options.addOption("m", "method", true, "multiplication method (single_thread, thread_by_cell, thread_by_row, " +
                "thread_by_cell_group)");
        options.addOption("c", "thread_count", true, "count of using thread");
        options.addOption("g", "cell_group", true, "number cells in executable group");
        options.addOption("w", "warmup", true, "count of warmup iterations");
        options.addOption("i", "iterations", true, "count of main iterations");
        options.addOption("b", "bound", true, "bound of generation elements");
        CommandLineParser cmdLinePosixParser = new DefaultParser();
        commandLine = cmdLinePosixParser.parse(options, args);
    }

    public boolean autoGeneration(){
        return commandLine.hasOption("a");
    }

    public int getMatrixSize(){
        if(!commandLine.hasOption("n")){
            return 3;
        }
        return Integer.parseInt(commandLine.getOptionValue("n"));
    }

    public String getMultiplicationType(){
        if(!commandLine.hasOption("t")){
            return "matrix";
        }
        return commandLine.getOptionValue("t");
    }

    public String getMultiplicationMethod(){
        if(!commandLine.hasOption("m")){
            return "single_thread";
        }
        return commandLine.getOptionValue("m");
    }

    public int getThreadCount(){
        if(Objects.equals(getMultiplicationMethod(), "single_thread")){
            return 1;
        }
        if(!commandLine.hasOption("c")){
            return 4;
        }
        return Integer.parseInt(commandLine.getOptionValue("c"));
    }

    public int getCellGroup(){
        if(!Objects.equals(getMultiplicationMethod(), "thread_by_cell_group")){
            return 0;
        }
        if(!commandLine.hasOption("g")){
            return 1;
        }
        return Integer.parseInt(commandLine.getOptionValue("g"));
    }

    public int getWarmupCount(){
        if(!commandLine.hasOption("w")){
            return 0;
        }
        return Integer.parseInt(commandLine.getOptionValue("w"));
    }

    public int getIterationsCount(){
        if(!commandLine.hasOption("i")){
            return 1;
        }
        return Integer.parseInt(commandLine.getOptionValue("i"));
    }

    public int getBound(){
        if(!commandLine.hasOption("b")){
            return 1000;
        }
        return Integer.parseInt(commandLine.getOptionValue("b"));
    }
}
