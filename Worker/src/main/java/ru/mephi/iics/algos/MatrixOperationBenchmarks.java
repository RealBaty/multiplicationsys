package ru.mephi.iics.algos;
import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

@BenchmarkMode({Mode.AverageTime})
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Fork(value = 1, warmups = 1)
@Measurement(iterations = 3, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Warmup(iterations = 2, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Timeout(time = 1, timeUnit = TimeUnit.SECONDS)
@Threads(value = 1)
public class MatrixOperationBenchmarks {

    @Param({"10000"})
    public int n;
    public int c = 1345;
    @Param({"2", "3", "4", "5"})
    public int threadsCount;
    public int[][] firstMatrix, secondMatrix;
    public MatrixOperations matrixOperations;

    public MatrixOperationBenchmarks(){
        matrixOperations = new MatrixOperations();
    }

    @Setup(Level.Trial)
    public void Setup(){
        firstMatrix = MatrixOperations.createRandomMatrix(n);
        secondMatrix = MatrixOperations.createRandomMatrix(n);
        matrixOperations = new MatrixOperations(threadsCount);
    }

    @State(Scope.Benchmark)
    public static class CountCells {
        @Param({"5", "10", "30", "50", "70", "100", "500", "1000"})
        public int countCells;
    }

    //@Benchmark
    public int[][] multiplyMatrixByNum() {
        return matrixOperations.multiplyMatrixByNum(firstMatrix, c);
    }

    @Benchmark
    public int[][] multiplyMatrixByNumMultiTreadsRow() throws InterruptedException {
        return matrixOperations.multiplyMatrixByNumMultiTreadsRow(firstMatrix, c);
    }

    //@Benchmark
    public int[][] multiplyMatrixByNumMultiTreadsCell() throws InterruptedException {
        return matrixOperations.multiplyMatrixByNumMultiTreadsCell(firstMatrix, c);
    }

    @Benchmark
    public int[][] multiplyMatrixByNumMultiTreadsMultiCells(CountCells countCells) throws InterruptedException {
        return matrixOperations.multiplyMatrixByNumMultiTreadsMultiCells(firstMatrix, c, countCells.countCells);
    }

    //@Benchmark
    public int[][] multiplyMatrices() {
        return matrixOperations.multiplyMatrices(firstMatrix, secondMatrix);
    }

    @Benchmark
    public int[][] multiplyMatricesMultiTreadsRow() throws InterruptedException {
        return matrixOperations.multiplyMatricesMultiTreadsRow(firstMatrix, secondMatrix);
    }

    //@Benchmark
    public int[][] multiplyMatricesMultiTreadsCell() throws InterruptedException {
        return matrixOperations.multiplyMatricesMultiTreadsCell(firstMatrix, secondMatrix);
    }

    @Benchmark
    public int[][] multiplyMatricesMultiTreadsMultiCells(CountCells countCells) throws InterruptedException {
        return matrixOperations.multiplyMatricesMultiTreadsMultiCells(firstMatrix, secondMatrix, countCells.countCells);
    }

}
