package ru.mephi.iics.thread;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;

public class ThreadPool{
    private final Queue<Runnable> workQueue = new ConcurrentLinkedQueue<>();
    private volatile boolean isRunning = true, doNotAdd = false;
    CountDownLatch waitGroup;

    public ThreadPool(int threadsCount) {
        waitGroup = new CountDownLatch(threadsCount);
        for(int i = 0; i < threadsCount; ++i){
            new Thread(new TaskWorker(), "Thread " + i).start();
        }
    }

    public void execute(Runnable command) {
        if (isRunning && command != null && !doNotAdd) {
            workQueue.offer(command);
        }
    }

    public void shutdown() throws InterruptedException {
        isRunning = false;
        waitGroup.await();
    }

    public void doAllAndShutdown() throws InterruptedException {
        doNotAdd = true;
        waitGroup.await();
    }



    private final class TaskWorker implements Runnable {

        @Override
        public void run() {
            while (isRunning) {
                Runnable nextTask = workQueue.poll();
                if (nextTask != null) {
                    nextTask.run();
                }
                else if (doNotAdd){
                    break;
                }
            }
            waitGroup.countDown();
        }
    }
}