package ru.mephi.iics.worker;

import com.github.daniel.shuy.kafka.protobuf.serde.KafkaProtobufDeserializer;
import com.github.daniel.shuy.kafka.protobuf.serde.KafkaProtobufSerializer;
import org.apache.commons.cli.ParseException;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import ru.mephi.iics.algos.MatrixOperations;
import ru.mephi.iics.proto.WorkerProto;

import java.util.Objects;
import java.util.Properties;

public class Program {


    private static String groupId;
    private static String rsTopic;
    private static String bootstrapServers;
    private static String rqTopic;

    private static void extractEnvVars() {
        groupId = System.getenv("MSS_KAFKA_GROUP_ID");
        if (groupId == null) {
            groupId = "mss.workers";
        }

        rsTopic = System.getenv("MSS_KAFKA_RS_TOPIC");
        if (rsTopic == null) {
            rsTopic = "mss.worker.rs";
        }

        bootstrapServers = System.getenv("MSS_KAFKA_BOOTSTRAP_SERVERS");
        if (bootstrapServers == null) {
            bootstrapServers = "localhost:29092";
        }

        rqTopic = System.getenv("MSS_KAFKA_RQ_TOPIC");
        if (rqTopic == null) {
            rqTopic = "mss.worker.rq";
        }
    }

    private static KafkaProducer<String, WorkerProto.WorkerRs> generateProducer() {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", bootstrapServers);
        return new KafkaProducer<>(properties, new StringSerializer(),
                new KafkaProtobufSerializer<>());
    }

    private static KafkaConsumer<String, WorkerProto.WorkerRq> generateConsumer() {
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", bootstrapServers);
        properties.setProperty("group.id", groupId);
        properties.setProperty("auto.offset.reset","latest");
        return new KafkaConsumer<>(properties,
                new StringDeserializer(),
                new KafkaProtobufDeserializer<>(WorkerProto.WorkerRq.parser()));
    }

    public static void main(String[] args) throws ParseException, InterruptedException {
        extractEnvVars();
        var producer = generateProducer();
        var consumer = generateConsumer();
        var config = new WorkerConfiguration(args);
        MatrixOperations matrixOperations = new MatrixOperations(config.getThreadCount());
        if(Objects.equals(config.getMultiplicationMethod(), "thread_by_cell")){
            Service.Serve(rqTopic, consumer, rsTopic, producer, matrixOperations::multiplyMatricesMultiTreadsCell);
        }
        else if(Objects.equals(config.getMultiplicationMethod(), "thread_by_row")){
            Service.Serve(rqTopic, consumer, rsTopic, producer, matrixOperations::multiplyMatricesMultiTreadsRow);
        }
        else if(Objects.equals(config.getMultiplicationMethod(), "thread_by_cell_group")){
            Service.Serve(rqTopic, consumer, rsTopic, producer,
                    (int[][] first, int[][] second) -> matrixOperations.multiplyMatricesMultiTreadsMultiCells(first, second,
                            config.getCellGroup()));
        }
        else{
            Service.Serve(rqTopic, consumer, rsTopic, producer, matrixOperations::multiplyMatrices);
        }
    }
}
