package ru.mephi.iics.worker;

import ru.mephi.iics.proto.MatrixProto;


public class Algorithms {

    private static int[][] getSimpleObj(MatrixProto.Matrix matrix){
        var dimX = matrix.getDimX();
        var dimY = matrix.getDimY();
        int[][] res = new int[dimX][dimY];
        for (var i = 0; i < dimX; ++i)
        {
            for (var j = 0; j < dimY; ++j)
            {
                res[i][j] = matrix.getLines(i).getColumns(j);
            }
        }
        return res;
    }

    private static MatrixProto.Matrix getSerializableObj(int[][] matrix){
        var resultBuilder = MatrixProto.Matrix.newBuilder().setDimX(matrix.length).setDimY(matrix[0].length);
        for (var i = 0; i < matrix.length; ++i)
        {
            var lineBuilder = MatrixProto.Matrix.Line.newBuilder();
            for (var j = 0; j < matrix[0].length; ++j)
            {
                lineBuilder.addColumns(matrix[i][j]);
            }
            resultBuilder.addLines(lineBuilder.build());
        }
        return resultBuilder.build();
    }

    public static MatrixProto.Matrix Sum(MatrixProto.PairMatrix ms, multiplicative m) throws InterruptedException {
        var left = getSimpleObj(ms.getLeft());
        var right = getSimpleObj(ms.getRight());

        var res = m.multiplication(left, right);

        return getSerializableObj(res);
    }

    public interface multiplicative{
        public int[][] multiplication(int[][] a, int[][] b) throws InterruptedException;
    }
}
