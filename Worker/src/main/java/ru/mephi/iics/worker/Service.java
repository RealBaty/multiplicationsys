package ru.mephi.iics.worker;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.time.Duration;
import java.util.Collections;

import ru.mephi.iics.proto.WorkerProto;

public class Service {
    public static void Serve(
            String                    rqTopic,
            KafkaConsumer<String, WorkerProto.WorkerRq> consumer,
            String                    rsTopic,
            KafkaProducer<String, WorkerProto.WorkerRs> producer,
            Algorithms.multiplicative m
    ) throws InterruptedException {
        consumer.subscribe(Collections.singleton(rqTopic));
        while (true)
        {
            ConsumerRecords<String, WorkerProto.WorkerRq> records = consumer.poll(Duration.ofMillis(1000));
            if (records.count() == 0) {
                continue;
            }
            for(var record: records){
                var resultMatrix = Algorithms.Sum(record.value().getRqData(), m);
                var correlationId = record.value().getCorrelationId();
                var workerRs = WorkerProto.WorkerRs.newBuilder().setRsData(resultMatrix).setCorrelationId(correlationId).build();
                ProducerRecord<String, WorkerProto.WorkerRs> producerRecord = new ProducerRecord<>(rsTopic, null, workerRs);
                producer.send(producerRecord);
            }
        }
    }

}
