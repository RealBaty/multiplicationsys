package ru.mephi.iics.worker;

import org.apache.commons.cli.*;

import java.util.Objects;

public class WorkerConfiguration {

    private final CommandLine commandLine;

    public WorkerConfiguration(String[] args) throws ParseException {
        Options options = new Options();
        options.addOption("m", "method", true, "multiplication method (single_thread, thread_by_cell, thread_by_row, " +
                "thread_by_cell_group)");
        options.addOption("c", "thread_count", true, "count of using thread");
        options.addOption("g", "cell_group", true, "number cells in executable group");
        CommandLineParser cmdLinePosixParser = new DefaultParser();
        commandLine = cmdLinePosixParser.parse(options, args);
    }

    public String getMultiplicationMethod(){
        if(!commandLine.hasOption("m")){
            return "single_thread";
        }
        return commandLine.getOptionValue("m");
    }

    public int getThreadCount(){
        if(Objects.equals(getMultiplicationMethod(), "single_thread")){
            return 1;
        }
        if(!commandLine.hasOption("c")){
            return 4;
        }
        return Integer.parseInt(commandLine.getOptionValue("c"));
    }

    public int getCellGroup(){
        if(!Objects.equals(getMultiplicationMethod(), "thread_by_cell_group")){
            return 0;
        }
        if(!commandLine.hasOption("g")){
            return 1;
        }
        return Integer.parseInt(commandLine.getOptionValue("g"));
    }
}
